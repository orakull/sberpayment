//
//  DatePickerCell.swift
//  SberPayment
//
//  Created by Руслан Ольховка on 25.04.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

class DatePickerCell: UITableViewCell {

	@IBOutlet weak var datePicker: UIDatePicker!
	
	@IBAction func clear(_ sender: Any) {
		onClear?()
	}
	@IBAction func apply(_ sender: Any) {
		onApply?(datePicker.date)
	}
	
	var onClear: (() -> ())?
	var onApply: ((_ date: Date) -> ())?
	
}
