//
//  LabelButtonCell.swift
//  SberPayment
//
//  Created by Руслан Ольховка on 25.04.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

class LabelButtonCell: UITableViewCell {

	@IBOutlet weak var label: UILabel!
	@IBOutlet weak var button: UIButton!

}
