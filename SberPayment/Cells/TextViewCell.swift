//
//  TextViewCell.swift
//  SberPayment
//
//  Created by Руслан Ольховка on 25.04.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

class TextViewCell: UITableViewCell {

	@IBOutlet weak var textField: MyTextView!
	
}
