//
//  TwoFieldsCell.swift
//  SberPayment
//
//  Created by Руслан Ольховка on 25.04.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

class TwoFieldsCell: UITableViewCell {
	
	@IBOutlet weak var leftLabel: UILabel!
	@IBOutlet weak var leftField: LinedTextField!
	@IBOutlet weak var rigthLabel: UILabel!
	@IBOutlet weak var rightField: LinedTextField!
	@IBOutlet weak var refreshButton: UIButton!
	@IBOutlet weak var middleConstraint: NSLayoutConstraint!
	
	override func layoutSubviews() {
		super.layoutSubviews()
		middleConstraint.constant = bounds.width * 0.5
	}

}
