//
//  LinedTextField.swift
//  SberPayment
//
//  Created by Руслан Ольховка on 25.04.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

@IBDesignable
class LinedTextField: UITextField {

	lazy var line: CALayer = {
		let line = CALayer()
		line.backgroundColor = tintColor.cgColor
		layer.addSublayer(line)
		return line
	}()
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		updateLine()
	}
	
	func updateLine() {
		line.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: 1)
	}

}
