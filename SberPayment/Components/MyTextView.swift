//
//  MyTextView.swift
//  SberPayment
//
//  Created by Руслан Ольховка on 25.04.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

@IBDesignable
class MyTextView: UITextView {

	@IBInspectable
	var placeHolder: String = "" {
		didSet {
			placeHolderLabel.text = placeHolder
			setNeedsLayout()
		}
	}

	private let placeHolderLabel = UILabel()
	
	override func awakeFromNib() {
		super.awakeFromNib()
		setup()
	}
	
	func setup() {
		placeHolderLabel.lineBreakMode = .byWordWrapping
		placeHolderLabel.numberOfLines = 0
		placeHolderLabel.font = self.font
		addSubview(placeHolderLabel)
		
		NotificationCenter.default.addObserver(self, selector: #selector(onTextChange), name: NSNotification.Name.UITextViewTextDidChange, object: nil)
	}
	
	override func layoutSubviews() {
		super.layoutSubviews()
		
		let placeHolderSize = placeHolderLabel.sizeThatFits(contentSize)
		placeHolderLabel.frame = CGRect(origin: CGPoint(x: 6, y: 8), size: placeHolderSize)
	}
	
	@objc private func onTextChange() {
		guard !placeHolder.isEmpty else { return }
		
		self.placeHolderLabel.alpha = self.text.isEmpty ? 1 : 0
	}
	
}
