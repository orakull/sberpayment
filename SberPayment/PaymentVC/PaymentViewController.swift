//
//  PaymentViewController.swift
//  SberPayment
//
//  Created by Руслан Ольховка on 25.04.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {

	@IBOutlet weak var table: UITableView!
	
	lazy var dateFormatter: DateFormatter = {
		let dateFormatter = DateFormatter()
		dateFormatter.dateStyle = .short
		dateFormatter.timeStyle = .none
		dateFormatter.locale = Locale(identifier: "ru_RU")
		return dateFormatter
	}()
	
	lazy var cell0: TwoFieldsCell = {
		let cell: TwoFieldsCell = table.dequeueReusableCell()
		cell.leftLabel.text = "Документ"
		cell.leftField.text = "№ 3"
		cell.rigthLabel.text = "Дата поручения"
		cell.rightField.text = "03.08.2017"
		cell.rightField.inputView = UIView()
		cell.rightField.addTarget(self, action: #selector(onEditBegin), for: .editingDidBegin)
		cell.rightField.addTarget(self, action: #selector(onEditEnd), for: .editingDidEnd)
		return cell
	}()
	
	lazy var cell1: DatePickerCell = {
		let cell: DatePickerCell = table.dequeueReusableCell()
		cell.onClear = { [weak self] in
			self?.cell0.rightField.text = ""
		}
		cell.onApply = { [unowned self] date in
			let dateStr = self.dateFormatter.string(from: date)
			self.cell0.rightField.text = dateStr
		}
		return cell
	}()
	
	lazy var cell2: TwoFieldsCell = {
		let cell: TwoFieldsCell = table.dequeueReusableCell()
		cell.leftLabel.text = "Сумма в рублях"
		cell.leftField.text = "0,00 ₽"
		cell.rigthLabel.text = "Расчет НДС"
		cell.rightField.text = "Расчет по % (1)"
		cell.refreshButton.removeFromSuperview()
		cell.accessoryType = .disclosureIndicator
		return cell
	}()
	
	lazy var cell3: LabelButtonCell = {
		let cell: LabelButtonCell = table.dequeueReusableCell()
		cell.label.text = "Получатель"
		return cell
	}()
	
	lazy var cell4: TextViewCell = {
		let cell: TextViewCell = table.dequeueReusableCell()
		cell.textField.placeHolder = "Введите наименование или выберите из справочника"
		cell.accessoryType = .disclosureIndicator
		return cell
	}()
	
	lazy var cells: [UITableViewCell] = {
		return [cell0, cell2, cell3, cell4]
	}()
	
	override func viewDidLoad() {
        super.viewDidLoad()
		
		title = "Платеж в бюджет"
		
		table.tintColor = #colorLiteral(red: 0.2745098039, green: 0.5764705882, blue: 0.2980392157, alpha: 1)
		table.backgroundColor = #colorLiteral(red: 0.9176470588, green: 0.9607843137, blue: 0.9215686275, alpha: 1)
		
		table.register(cellType: TwoFieldsCell.self)
		table.register(cellType: LabelButtonCell.self)
		table.register(cellType: TextViewCell.self)
		table.register(cellType: DatePickerCell.self)
    }
}

extension PaymentViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return cells.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		return cells[indexPath.row]
	}
	
	@objc func onEditBegin() {
		cells.insert(cell1, at: 1)
		table.insertRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
	}
	@objc func onEditEnd() {
		cells.remove(at: cells.index(of: cell1)!)
		table.deleteRows(at: [IndexPath(row: 1, section: 0)], with: .automatic)
	}
}

extension PaymentViewController: UITableViewDelegate {
}
