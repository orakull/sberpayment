//
//  Extensions.swift
//  SberPayment
//
//  Created by Руслан Ольховка on 26.04.2018.
//  Copyright © 2018 Руслан Ольховка. All rights reserved.
//

import UIKit

extension UITableViewCell {
	static var reuseIdentifier: String {
		return String(describing: self)
	}
}
extension UITableView {
	func register<T: UITableViewCell>(cellType: T.Type) {
		let nibName = String(describing: cellType)
		let nib = UINib(nibName: nibName, bundle: nil)
		self.register(nib, forCellReuseIdentifier: T.reuseIdentifier)
	}
	func dequeueReusableCell<T: UITableViewCell>(offCellType cellType: T.Type? = nil) -> T {
		return dequeueReusableCell(withIdentifier: T.reuseIdentifier) as! T
	}
}
